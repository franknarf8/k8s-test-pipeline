var http = require('http');
var compareVersions = require('compare-versions');

var handleRequest = function (request, response) {
  console.log('Received request for URL: ' + request.url);
  response.writeHead(200);
  response.end('Hello World!' + compareVersions('10.1.8', '10.0.4'));
};
var www = http.createServer(handleRequest);
www.listen(8080);
